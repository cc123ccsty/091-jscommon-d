import * as echarts from "echarts";

class DrawImageOperator {
    /**
     *
     * @param el 需要绑定的元素ref
     * @param titleText 标题
     * @param xDescriptionArr x轴描述的数组
     * @param series series中包含的是图形结构
     * 这里可以适用于柱状图(包含列族柱状图)和折线图以及散点图
     */
    initBarImage(el, titleText, xDescriptionArr, series) {
        let myEcharts = echarts.init(el)
        let option = {
            title: {
                text: titleText
            },
            tooltip: {},
            legend: {},
            xAxis: {
                data: xDescriptionArr
            },
            yAxis: {},
            series: series
        }
        myEcharts.setOption(option)
    }

    /**
     *
     * @param el 需要绑定的元素ref
     * @param titleText 标题
     * @param yDescriptionArr y轴描述的数组
     * @param series series中包含的是图形结构
     */
    initBarImageY(el, titleText, yDescriptionArr, series) {
        let myEcharts = echarts.init(el)
        let option = {
            title: {
                text: titleText
            },
            tooltip: {},
            legend: {},
            xAxis: {},
            yAxis: {
                data: yDescriptionArr
            },
            series: series
        }
        myEcharts.setOption(option)
    }

    /**
     *
     * @param el        需要绑定的元素ref
     * @param titleText  标题
     * @param xDescription x轴描述的数组
     * @param data       要展示的数据
     * @param scalePercent 单轴散点图的散点的放缩比例
     * @param color       散点图颜色
     */
    initSingleAxisImage(el,
                        titleText = "",
                        xDescription,
                        data,
                        scalePercent,
                        color = "skyblue") {
        let myEcharts = echarts.init(el)
        let option = {
            title: {
                text: titleText
            },
            tooltip: {},
            legend: {},
            singleAxis: {
                type: "category",
                coordinateSystem: "singleAxis",
                data: xDescription
            },
            series: [{
                type: "scatter",
                coordinateSystem: "singleAxis",
                data: data,
                color: color,
                symbolSize: function (data) {
                    return data / scalePercent
                }
            }]
        }
        myEcharts.setOption(option)
    }

    initSingleAxisImageHelper(
        elArray,
        colorArray,
        bucket,
        attribute,
        scalePercent,
        title = ""
    ) {
        let num = 0
        //如果长度不够,就直接添加默认颜色
        if (colorArray.length < bucket.size) {
            for (let i = colorArray.length; i < bucket.size; i++) {
                colorArray.push("skyblue")
            }
        }

        bucket.forEach((item, key) => {
            this.initSingleAxisImage(
                elArray[num],
                num === 0 ? title : "",
                item[attribute],
                item[attribute],
                scalePercent,
                colorArray[num]
            )
            num++;
        })
    }

    /**
     *
     * @param el       需要绑定的元素ref
     * @param titleText 标题
     * @param legendPosition  legend的位置
     * @param radius 饼图大小
     * @param data 要展示的数据
     * @param formatter 需要自定义的格式化数据{a}:{b}:{c}
     */
    initPieImage(el,
                 titleText = "",
                 legendPosition = "top",
                 radius = "30%",
                 data,
                 formatter = "") {
        let myEcharts = echarts.init(el)
        let option = {
            title: {
                text: titleText
            },
            tooltip: {},
            legend: {
                top: legendPosition
            },
            series: [{
                type: "pie",
                data: data,//data的形式为[{name:xxx,value:xxx},{name:xxx,value:xxx}]
                radius: radius,
                label: {
                    formatter: formatter
                }
            }]
        }
        myEcharts.setOption(option)
    }

    /**
     *
     * @param el       需要绑定的元素ref
     * @param titleText 标题
     * @param legendPosition  legend的位置
     * @param series  要存入的数据结构,展示的数据
     */
    initSimplePieImage(el,
                       titleText = "",
                       legendPosition = "top",
                       series) {
        let myEcharts = echarts.init(el)
        let option = {
            title: {
                text: titleText
            },
            tooltip: {},
            legend: {
                top: legendPosition
            },
            series: series
        }
        myEcharts.setOption(option)
    }

    /**
     * 绘制折柱混合图,这里实现了双x轴
     * @param el  要绑定元素
     * @param titleText 标题
     * @param xDescriptionArr1  第一个x轴描述数组
     * @param xDescriptionArr2  第二个x轴描述数组
     * @param series  需要展示的series数据结构
     */
    initLineAndBarImage(el, titleText, xDescriptionArr1, xDescriptionArr2, series) {
        let myEcharts = echarts.init(el)
        let option = {
            title: {
                text: titleText
            },
            tooltip: {},
            legend: {},
            xAxis: [
                {
                    data: xDescriptionArr1
                },
                {
                    data: xDescriptionArr2
                }
            ],
            yAxis: {},
            series: series
        }
        myEcharts.setOption(option)
    }
}

export default DrawImageOperator
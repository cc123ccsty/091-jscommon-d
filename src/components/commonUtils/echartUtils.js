import SeriesOperators from "./seriesOperators";
import DrawImageOperator from "./DrawImageOperator";
import * as echarts from "echarts";

class echartsUtils {
    static seriesOperator = new SeriesOperators()
    static drawImageOperator = new DrawImageOperator()

    /**
     *
     * @param el 需要绑定的元素ref
     * @param titleText 标题
     * @param xDescriptionArr x轴描述的数组
     * @param series series中包含的是图形结构
     * 可以适用于line,bar,scatter等图片
     */
    static initBarImage(el, titleText, xDescriptionArr, series) {
        this.drawImageOperator.initBarImage(
            el,
            titleText,
            xDescriptionArr,
            series
        )
    }

    /**
     *
     * @param el 需要绑定的元素ref
     * @param titleText 标题
     * @param yDescriptionArr x轴描述的数组
     * @param series series中包含的是图形结构
     */
    static initBarImageY(el, titleText, yDescriptionArr, series) {
        this.drawImageOperator.initBarImageY(
            el,
            titleText,
            yDescriptionArr,
            series
        )
    }

    /**
     *
     * @param resultBucket  需要渲染的bucket映射
     * @param renderAttribute 需要渲染的映射中属性
     * @param isShowLabel   是否显示标签
     * @param position      指定标签位置
     * @return {Array[]} 返回的是series数组,里面存放图像类型数据结构
     */
    static initBarSeries(resultBucket,
                         renderAttribute,
                         isShowLabel = true,
                         position = "top") {

        //返回数组的画图结构
        return this.seriesOperator.initBarSeries(
            resultBucket,
            renderAttribute,
            isShowLabel,
            position
        )
    }

    /**
     *
     * @param el        需要绑定的元素ref
     * @param titleText  标题
     * @param xDescription x轴描述的数组
     * @param data       要展示的数据
     * @param scalePercent 单轴散点图的散点的放缩比例
     * @param color       散点图颜色
     */
    static initSingleAxisImage(el,
                               titleText = "",
                               xDescription,
                               data,
                               scalePercent,
                               color = "skyblue") {
        this.drawImageOperator.initSingleAxisImage(
            el,
            titleText,
            xDescription,
            data,
            scalePercent,
            color
        )
    }

    /**
     *
     * @param el       需要绑定的元素ref
     * @param titleText 标题
     * @param legendPosition  legend的位置
     * @param data 要展示的数据
     */
    static initPieImage(el,
                        titleText = "",
                        legendPosition = "top",
                        data) {
        this.drawImageOperator.initPieImage(
            el,
            titleText,
            legendPosition,
            data
        )
    }

    /**
     * 获取临时set
     * @return {Set<any>} 返回临时set中的值
     */
    static getTempSet() {
        return this.seriesOperator.getTempSet()
    }

    /**
     *
     * @param resultBucket  需要渲染的bucket映射
     * @param renderAttribute 需要渲染的映射中属性
     * @param isShowLabel   是否显示标签
     * @param position      指定标签位置
     * @return {Array[]} 返回的是series数组,里面存放图像类型数据结构
     */
    static initLineSeries(resultBucket,
                          renderAttribute,
                          isShowLabel = true,
                          position = "top") {
        return this.seriesOperator.initLineSeries(
            resultBucket,
            renderAttribute,
            isShowLabel,
            position
        )
    }

    /**
     *
     * @param data   要渲染的数据
     * @param isShowLabel 是否显示label
     * @param position   label显示的位置[top,bottom,left,right]
     * 只返回一个对象结构,存储的是line折线图
     * @return {{data: *, name: *, label: {show: boolean, position: string}, type: string}}
     */
    static initOnlyLineSeries(data,
                              isShowLabel = true,
                              position = "top") {
        return this.seriesOperator.initOnlyLineSeries(
            data,
            isShowLabel,
            position
        )
    }

    /**
     * 绘制折柱混合图,这里实现了双x轴
     * @param el  要绑定元素
     * @param titleText 标题
     * @param xDescriptionArr1  第一个x轴描述数组
     * @param xDescriptionArr2  第二个x轴描述数组
     * @param series  需要展示的series数据结构
     */
    static initLineAndBarImage(el, titleText, xDescriptionArr1, xDescriptionArr2, series) {
        this.drawImageOperator.initLineAndBarImage(
            el,
            titleText,
            xDescriptionArr1,
            xDescriptionArr2,
            series
        )
    }

    /**
     *
     * @param el       需要绑定的元素ref
     * @param titleText 标题
     * @param series  要存入的数据结构,展示的数据
     * @param legendPosition  legend的位置
     */
    static initSimplePieImage(el,
                              titleText = "",
                              series,
                              legendPosition = "top"
    ) {
        this.drawImageOperator.initSimplePieImage(
            el,
            titleText,
            legendPosition,
            series
        )
    }

    /**
     *
     * @param bucket  渲染series的数据源
     * @param radius  饼图大小 如:"30%","60%"等等
     * @param labelFormatter 饼图的label格式化信息  值可以为:{a}:{b}:{c}:{d}
     * @return {[]} 返回一个新的series数组
     */
    static initComplexPieSeries(
        bucket,
        radius,
        labelFormatter
    ) {
        return this.seriesOperator.initComplexPieSeries(
            bucket,
            radius,
            labelFormatter
        )
    }

    /**
     *
     * @param series 需要指定位置的pie对应的series结构
     * @return {*} 返回一个赋值位置属性(center)后的series
     */
    static initPiePosition(series) {
        return this.seriesOperator.initPiePosition(series)
    }

    /***
     *
     * @param elArray 要渲染的标签ref列表
     * @param colorArray 要渲染的颜色列表
     * @param bucket  要渲染的数据源
     * @param attribute 要渲染的数据属性
     * @param scalePercent 指定单轴散点图的散点大小
     * @param title 指定图像的标题
     */
    static initSingleAxisImageHelper(
        elArray,
        colorArray,
        bucket,
        attribute,
        scalePercent,
        title = ""
    ) {
        this.drawImageOperator.initSingleAxisImageHelper(elArray,
            colorArray,
            bucket,
            attribute,
            scalePercent,
            title)
    }
}

export default echartsUtils
class SeriesOperators {
    tempSet = new Set()

    /**
     *
     * @param resultBucket  需要渲染的bucket映射
     * @param renderAttribute 需要渲染的映射中属性
     * @param isShowLabel   是否显示标签
     * @param position      指定标签位置
     * @param isNeedToReturnSet 是否需要返回一个Set
     * @param returnSetElem  如果需要返回一个set,需要存入set的元素属性或对象
     * @return {Array[]} 返回的是series数组,里面存放图像类型数据结构
     */
    initBarSeries(resultBucket,
                  renderAttribute,
                  isShowLabel = true,
                  position = "top",
                  isNeedToReturnSet = false,
                  returnSetElem = ""
    ) {
        //创建一个数组
        let series = []
        let localSet = new Set();
        resultBucket.forEach((item, key) => {
            //如果要存key,那就存key,否则存指定对象
            if (returnSetElem.toLowerCase() === "key") {
                localSet.add(key)
            } else {
                localSet.add(item[returnSetElem])
            }
            series.push({//将数据渲染到series中
                type: "bar",
                data: item.map(it => it[renderAttribute]),
                name: key,
                label: {
                    show: isShowLabel,
                    position: position
                }
            })
        })
        if (isNeedToReturnSet) {
            this.tempSet = localSet
        }
        //返回数组的画图结构
        return series
    }

    //获取临时set
    /**
     * 获取临时set
     * @return {Set<any>} 返回临时set中的值
     */
    getTempSet() {
        if (this.tempSet.size <= 0) {
            throw new Error("临时set没有数据,无法获取,请先调用initBarSeries方法后再使用该方法")
        }
        return this.tempSet
    }

    /**
     *
     * @param resultBucket  需要渲染的bucket映射
     * @param renderAttribute 需要渲染的映射中属性
     * @param isShowLabel   是否显示标签
     * @param position      指定标签位置
     * @return {Array[]} 返回的是series数组,里面存放图像类型数据结构
     */
    initLineSeries(resultBucket,
                   renderAttribute,
                   isShowLabel = true,
                   position = "top") {
        //创建一个数组
        let series = []
        resultBucket.forEach((item, key) => {
            series.push({//将数据渲染到series中
                type: "line",
                data: item.map(it => it[renderAttribute]),
                name: key,
                label: {
                    show: isShowLabel,
                    position: position
                }
            })
        })
        //返回数组的画图结构
        return series
    }

    /**
     *
     * @param data   要渲染的数据
     * @param isShowLabel 是否显示label
     * @param position   label显示的位置[top,bottom,left,right]
     * 只返回一个对象结构,存储的是line折线图
     * @return {{data: *, name: *, label: {show: boolean, position: string}, type: string}}
     */
    initOnlyLineSeries(data,
                       isShowLabel = true,
                       position = "top") {
        return {
            type: "line",
            data: data,
            label: {
                show: isShowLabel,
                position: position
            }
        }
    }

    /**
     *
     * @param bucket  渲染series的数据源
     * @param radius  饼图大小 如:"30%","60%"等等
     * @param labelFormatter 饼图的label格式化信息  值可以为:{a}:{b}:{c}:{d}
     * @return {[]} 返回一个新的series数组
     */
    initComplexPieSeries(
        bucket,
        radius,
        labelFormatter
    ) {
        let series = []
        //需要先将所有data的数据结构转换为{name:xxx,value:xxx的形式}
        bucket.forEach((item, key) => {
            series.push({
                type: "pie",
                data: item,
                name: key,
                radius: radius,
                label: {
                    formatter: labelFormatter
                }
            })
        })
        return series
    }

    /**
     *
     * @param series 需要指定位置的pie对应的series结构
     * @return {*} 返回一个赋值位置属性(center)后的series
     */
    initPiePosition(series) {
        let count = 0
        let y = 35;//初始化默认值
        for (let i = 0; i < series.length; i++) {
            //奇数["25%","35%"]
            //偶数["75%","35%"]
            if (i % 2 === 0) {
                //偶数
                series[i].center = ["25%", y + "%"]
            } else {
                /**具体样子
                 *    series.value[0].center = ["25%", "35%"]
                 series.value[1].center = ["75%", "35%"]
                 series.value[2].center = ["25%", "75%"]
                 series.value[3].center = ["75%", "75%"]
                 */
                //奇数
                series[i].center = ["75%", y + "%"]
                y += 40//如果出现奇数,所以就让y增加40,因为一行就俩个图
                //所以再奇数时,就需要换行,每换一行就需要对y进行增加
            }
            count++
        }
        return series
    }
}

export default SeriesOperators
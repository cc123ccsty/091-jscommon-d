import TimeProcessor from "./TimeProcessor";
import NumberHandler from "./NumberHandler";
import InitBucketHandler from "./InitBucketHandler";
import BuildBucket from "./BuildBucket";
import BuildArrayEnvironment from "./BuildArray";

class util {
    //用于存储临时数据的Set集合数据
    // static firstSet = new Set()
    // static secondSet = new Set()
    static timeProcessor = new TimeProcessor()
    static numberHandler = new NumberHandler()
    static initBucketHandler = new InitBucketHandler()
    static buildBucket = new BuildBucket()
    static buildArray = new BuildArrayEnvironment()
    /**
     * 用于计算两个时间相隔的时间,返回单位:毫秒
     * @param dt1 时间字符串1
     * @param dt2 时间字符串2
     * @returns {number}  相差的时间
     */
    static dateParse = (dt1, dt2) => {
        return this.timeProcessor.dateParse(dt1, dt2)
    }

    /**
     * 用于判断时间是否合法,如果dt2-dt1<0,代表时间不合法
     * @param dt1 时间字符串1
     * @param dt2 时间字符串2
     * 结束时间小于开始时间 为非法元素(当前已知过滤元素的规则)
     * true为合法 false为非法
     * @return {boolean} 时间是否合法
     */
    static  isValidDate = (dt1, dt2) => {
        return this.timeProcessor.isValidDate(dt1, dt2)
    }

    /**
     * 用于将时间从毫秒转换为秒
     * @param dt1 时间字符串1
     * @param dt2 时间字符串2
     * @return {number} 返回以秒为单位的时间
     */
    static getTimeSecond = (dt1, dt2) => {
        return this.timeProcessor.getTimeSecond(dt1, dt2)
    }

    /**
     *
     * @param num 数字
     * @param digits 保留几位小数
     * @return {string} 返回处理后的小数
     */
    static keepDecimal = (num, digits) => {
        return this.numberHandler.keepDecimal(num, digits)
    }
    /**
     * 格式化时间字符串为yyyy-MM-dd
     * @param dt 时间字符串
     * @return {*} 返回一个yyyy-MM-dd字符串时间
     */
    static formatTime = (dt) => {
        return this.timeProcessor.formatTime(dt)
    }

    /**
     * 内部方法,不要直接调用!!!!!!
     * @param needToBuildDataStruct 需要存入set的数据结构
     * @param separate 分隔符
     * @return {Map<any, any>} 这里返回一个map,主要用于初始化map结构
     */
    static _setComplexBucket(needToBuildDataStruct, separate) {
        return this.initBucketHandler._setComplexBucket(needToBuildDataStruct, separate)
    }

    //初始化Bucket映射中的元素
    /**
     * 初始化一个复杂的Map结构,复杂Map结构主要用于需要初始化全部元素到Map中的情况
     * 后续处理会在其他函数中处理
     * @param dataSource 数据源
     * @param arrAttribute 需要读取的数据结构中的属性 如:showFactChangeRecordList之类的
     * @param filterTimeArr 用于过滤时间的数组,只存放两个元素,一个起始时间,一个结束时间(顺序一定要按照起始时间,结束时间 来写)
     * @param insideSetAttributeArr 在内部存入Set集合的元素名称,只能存储两个元素,
     * 第一个元素为外层的数据属性(如baseMachineID,machineNo之类的元素),
     * 第二个元素为内层的数据属性(如showFactChangeRecordList,showFactEnvironmentData之类的内部元素)
     *
     * @param needToBuildDataStructFunc 需要初始化的bucket映射中的value属性结构,这里传入的是一个函数
     * 如下:
     * {
                    id: it,
                    status: tt,
                    runtime: 0//初始化持续时长
       }
     * @param separate 对bucket中key的分隔符,默认为------
     * @return {Map<*, *>} 这里返回一个map,主要用于初始化map结构
     */
    static initComplexBucket(dataSource,
                             arrAttribute,
                             filterTimeArr,
                             insideSetAttributeArr,
                             needToBuildDataStructFunc,
                             separate = "------") {

        return this.initBucketHandler.initComplexBucket(dataSource,
            arrAttribute,
            filterTimeArr,
            insideSetAttributeArr,
            needToBuildDataStructFunc,
            separate)
    }

    /**
     * 带有状态判断的初始化map结构
     * 初始化一个复杂的Map结构,复杂Map结构主要用于需要初始化全部元素到Map中的情况
     * 后续处理会在其他函数中处理
     * @param dataSource 数据源
     * @param arrAttribute 需要读取的数据结构中的属性 如:showFactChangeRecordList之类的
     * @param statusAttribute  需要判断的状态属性
     * @param equalStatus 需要等于的状态值
     * @param filterTimeArr 用于过滤时间的数组,只存放两个元素,一个起始时间,一个结束时间(顺序一定要按照起始时间,结束时间 来写)
     * @param insideSetAttributeArr 在内部存入Set集合的元素名称,只能存储两个元素,
     * 第一个元素为外层的数据属性(如baseMachineID,machineNo之类的元素),
     * 第二个元素为内层的数据属性(如showFactChangeRecordList,showFactEnvironmentData之类的内部元素)
     *
     * @param needToBuildDataStructFunc 需要初始化的bucket映射中的value属性结构,这里传入的是一个函数
     * 如下:
     * {
                    id: it,
                    status: tt,
                    runtime: 0//初始化持续时长
       }
     * @param separate 对bucket中key的分隔符,默认为------
     * @return {Map<*, *>} 这里返回一个map,主要用于初始化map结构
     */
    static initComplexBucketWithStatusJudgement(dataSource,
                                                arrAttribute,
                                                statusAttribute,
                                                equalStatus,
                                                filterTimeArr,
                                                insideSetAttributeArr,
                                                needToBuildDataStructFunc,
                                                separate = "------") {

        return this.initBucketHandler.initComplexBucketWithStatusJudgement(
            dataSource,
            arrAttribute,
            statusAttribute,
            equalStatus,
            filterTimeArr,
            insideSetAttributeArr,
            needToBuildDataStructFunc,
            separate
        )
    }

    //获取第一个set集合,一般存储公共属性
    static getFirstSet() {
        return this.initBucketHandler.getFirstSet();
    }

    //获取第二个set集合,一般存储属性内属性
    static getSecondSet() {
        return this.initBucketHandler.getSecondSet();
    }

    /**
     *
     * @param oldBucket 传入旧Bucket,新的Bucket映射需要根据旧的创建
     * @param keyIndex  新的Bucket的key索引,可选(0,1)
     * @param structArr 数据结构数组,存入新bucket中的数据结构,一定要和oldBucket中的数据结构名称一致
     * @param separate  字符串分隔符
     * @param isNeedAvg 是否需要计算平均值
     * @param AvgFields 需要计算平均值的数组属性,只传入两个元素,第一个元素为要保留属性
     * @param digitNum  需要四舍五入并保留小数的位数
     * @return {Map<any, any>} 返回新的Bucket
     */
    static buildNewBucket(oldBucket,
                          keyIndex,
                          structArr,
                          separate,
                          isNeedAvg = false,
                          AvgFields = [],
                          digitNum = 2) {
        return this.buildBucket.buildNewBucket(
            oldBucket,
            keyIndex,
            structArr,
            separate,
            isNeedAvg,
            AvgFields,
            digitNum
        )
    }

    /**
     *
     * @param oldBucket 用来创建新bucket的老bucket
     * @param keyIndex 需要作为key的索引
     * @param separate 分隔符
     * @param structArr 需要写入数据结构的属性,这里只接受两个参数
     * @return {Map<any, any>} 返回一个新的bucket
     */
    static buildANewPieBucket(
        oldBucket,
        keyIndex,
        separate,
        structArr
    ) {
        return this.buildBucket.buildANewPieBucket(
            oldBucket,
            keyIndex,
            separate,
            structArr
        );
    }

    /**
     * 直接构建一个数组,数组内只会添加某一个元素属性
     * @param dataSource 数据源
     * @param attribute  需要遍历的数据属性
     * @param insiderAttr 数据属性内部的属性,作为添加进入数组中的元素
     */
    static buildASimpleArrayEnvironment(dataSource, attribute, insiderAttr) {
        return this.buildArray.buildASimpleArrayEnvironment(
            dataSource,
            attribute,
            insiderAttr
        )
    }
}


export default util
import TimeProcessor from "./TimeProcessor";

class InitBucketHandler {
    firstSet = new Set()
    secondSet = new Set()
    timeProcessor = new TimeProcessor()

    /**
     * 内部方法,不要直接调用!!!!!!
     * @param needToBuildDataStruct 需要存入set的数据结构
     * @param separate 分隔符
     * @return {Map<any, any>} 这里返回一个map,主要用于初始化map结构
     */
    _setComplexBucket(needToBuildDataStruct, separate) {
        if (this.firstSet.size === 0 && this.secondSet.size === 0) {
            throw new Error("两个Set集合为空,不要直接调用本函数,本函数用于函数内调用")
        }
        let bucket = new Map()
        this.firstSet.forEach(it => {
            this.secondSet.forEach(tt => {
                //设备id+间隔符+状态
                let key = it + separate + tt
                bucket.set(key, needToBuildDataStruct(it, tt))
            })
        })
        return bucket
    }

    //初始化Bucket映射中的元素
    /**
     * 初始化一个复杂的Map结构,复杂Map结构主要用于需要初始化全部元素到Map中的情况
     * 后续处理会在其他函数中处理
     * @param dataSource 数据源
     * @param arrAttribute 需要读取的数据结构中的属性 如:showFactChangeRecordList之类的
     * @param filterTimeArr 用于过滤时间的数组,只存放两个元素,一个起始时间,一个结束时间(顺序一定要按照起始时间,结束时间 来写)
     * @param insideSetAttributeArr 在内部存入Set集合的元素名称,只能存储两个元素,
     * 第一个元素为外层的数据属性(如baseMachineID,machineNo之类的元素),
     * 第二个元素为内层的数据属性(如showFactChangeRecordList,showFactEnvironmentData之类的内部元素)
     *
     * @param needToBuildDataStructFunc 需要初始化的bucket映射中的value属性结构,这里传入的是一个函数
     * 如下:
     * {
                    id: it,
                    status: tt,
                    runtime: 0//初始化持续时长
       }
     * @param separate 对bucket中key的分隔符,默认为------
     * @return {Map<*, *>} 这里返回一个map,主要用于初始化map结构
     */
    initComplexBucket(dataSource,
                      arrAttribute,
                      filterTimeArr,
                      insideSetAttributeArr,
                      needToBuildDataStructFunc,
                      separate = "------") {
        this.firstSet = new Set()
        this.secondSet = new Set()
        dataSource.map(item => {
            item[arrAttribute].filter(t => this.timeProcessor.isValidDate(t[filterTimeArr[0]], t[filterTimeArr[1]])).map(it => {
                this.firstSet.add(item[insideSetAttributeArr[0]])
                //判断传入的元素是否为时间相关属性,如果是,对数据做处理,yyyy-MM-dd
                //这里已经初始化好了secondSet,直接获取就行
                if (insideSetAttributeArr[1].toLowerCase().includes("time")) {
                    this.secondSet.add(this.timeProcessor.formatTime(it[insideSetAttributeArr[1]]))
                } else {
                    this.secondSet.add(it[insideSetAttributeArr[1]])
                }
            })
        })
        //初始化bucket
        return this._setComplexBucket(needToBuildDataStructFunc, separate)
    }

    /**
     * 带有状态判断的初始化map结构
     * 初始化一个复杂的Map结构,复杂Map结构主要用于需要初始化全部元素到Map中的情况
     * 后续处理会在其他函数中处理
     * @param dataSource 数据源
     * @param arrAttribute 需要读取的数据结构中的属性 如:showFactChangeRecordList之类的
     * @param statusAttribute  需要判断的状态属性
     * @param equalStatus 需要等于的状态值
     * @param filterTimeArr 用于过滤时间的数组,只存放两个元素,一个起始时间,一个结束时间(顺序一定要按照起始时间,结束时间 来写)
     * @param insideSetAttributeArr 在内部存入Set集合的元素名称,只能存储两个元素,
     * 第一个元素为外层的数据属性(如baseMachineID,machineNo之类的元素),
     * 第二个元素为内层的数据属性(如showFactChangeRecordList,showFactEnvironmentData之类的内部元素)
     *
     * @param needToBuildDataStructFunc 需要初始化的bucket映射中的value属性结构,这里传入的是一个函数
     * 如下:
     * {
                    id: it,
                    status: tt,
                    runtime: 0//初始化持续时长
       }
     * @param separate 对bucket中key的分隔符,默认为------
     * @return {Map<*, *>} 这里返回一个map,主要用于初始化map结构
     */
    initComplexBucketWithStatusJudgement(dataSource,
                                         arrAttribute,
                                         statusAttribute,
                                         equalStatus,
                                         filterTimeArr,
                                         insideSetAttributeArr,
                                         needToBuildDataStructFunc,
                                         separate = "------") {
        this.firstSet = new Set()
        this.secondSet = new Set()
        dataSource.map(item => {
            item[arrAttribute].filter(t => this.timeProcessor.isValidDate(t[filterTimeArr[0]], t[filterTimeArr[1]]) && t[statusAttribute] === equalStatus).map(it => {
                this.firstSet.add(item[insideSetAttributeArr[0]])
                if (insideSetAttributeArr[1].toLowerCase().includes("time")) {
                    this.secondSet.add(this.timeProcessor.formatTime(it[insideSetAttributeArr[1]]))
                } else {
                    this.secondSet.add(it[insideSetAttributeArr[1]])
                }
            })
        })
        return this._setComplexBucket(needToBuildDataStructFunc, separate)
    }

    //获取第一个set集合,一般存储公共属性
    getFirstSet() {
        return this.firstSet;
    }

    //获取第二个set集合,一般存储属性内属性
    getSecondSet() {
        return this.secondSet;
    }
}

export default InitBucketHandler
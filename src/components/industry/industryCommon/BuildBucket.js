import NumberHandler from "./NumberHandler";

class BuildBucket {
    numberHandler = new NumberHandler()

    /**
     *
     * @param oldBucket 传入旧Bucket,新的Bucket映射需要根据旧的创建
     * @param keyIndex  新的Bucket的key索引,可选(0,1)
     * @param structArr 数据结构数组,存入新bucket中的数据结构,一定要和oldBucket中的数据结构名称一致
     * @param separate  字符串分隔符
     * @param isNeedAvg 是否需要计算平均值
     * @param AvgFields 需要计算平均值的数组属性,只传入两个元素,第一个元素为要保留属性
     * @param digitNum  需要四舍五入并保留小数的位数
     * @return {Map<any, any>} 返回新的Bucket
     */
    buildNewBucket(oldBucket,
                   keyIndex,
                   structArr,
                   separate,
                   isNeedAvg = false,
                   AvgFields = [],
                   digitNum = 2) {
        let resultBucket = new Map()
        // console.log(oldBucket)
        oldBucket.forEach((item, keys) => {
            let key = keys.split(separate)[keyIndex]
            let arr = []
            let obj = {}//每次循环都会清空对象
            //这里将指定的新bucket的数据结构进行渲染
            structArr.forEach(structItem => {
                if (isNeedAvg) {//如果需要计算平均值
                    if (item[structItem] === item[AvgFields[0]]) {
                        //说明到了要保留的元素,那就四舍五入
                        obj[AvgFields[0]] = this.numberHandler.keepDecimal((item[AvgFields[0]] / item[AvgFields[1]]), digitNum)
                    } else {
                        //没有到要保留元素,就直接添加
                        obj[structItem] = item[structItem]
                    }
                } else {
                    //不需要平均值
                    obj[structItem] = item[structItem]
                }
            })
            if (!resultBucket.get(key)) {
                //将对象的数据结构传入
                //第一次插入直接插入一个arr
                arr.push(obj)
                resultBucket.set(key, arr)
            } else {
                resultBucket.get(key).push(obj)
            }
        })
        return resultBucket
    }

    //构建一个新的Pie类型的Bucket,存储的数据为{name,value形式}
    /**
     *
     * @param oldBucket 用来创建新bucket的老bucket
     * @param keyIndex 需要作为key的索引
     * @param separate 分隔符
     * @param structArr 需要写入数据结构的属性,这里只接受两个参数
     * @return {Map<any, any>} 返回一个新的bucket
     */
    buildANewPieBucket(
        oldBucket,
        keyIndex,
        separate,
        structArr
    ) {
        let bucket = new Map()
        oldBucket.forEach((item, key) => {
            let arr = []
            let keys = key.split(separate)[keyIndex]
            if (!bucket.get(keys)) {
                arr.push({
                    name: item[structArr[0]],
                    value: item[structArr[1]]
                })
                bucket.set(keys, arr)
            } else {
                bucket.get(keys).push({
                    name: item[structArr[0]],
                    value: item[structArr[1]]
                })
            }
        })
        return bucket;
    }
}

export default BuildBucket
class TimeProcessor {
    /**
     * 用于计算两个时间相隔的时间,返回单位:毫秒
     * @param dt1 时间字符串1
     * @param dt2 时间字符串2
     * @returns {number}  相差的时间
     */
    dateParse(dt1, dt2) {
        return Date.parse(dt2) - Date.parse(dt1)
    }

    /**
     * 用于判断时间是否合法,如果dt2-dt1<0,代表时间不合法
     * @param dt1 时间字符串1
     * @param dt2 时间字符串2
     * 结束时间小于开始时间 为非法元素(当前已知过滤元素的规则)
     * true为合法 false为非法
     * @return {boolean} 时间是否合法
     */
    isValidDate(dt1, dt2) {
        return this.dateParse(dt1, dt2) >= 0
    }

    /**
     * 用于将时间从毫秒转换为秒
     * @param dt1 时间字符串1
     * @param dt2 时间字符串2
     * @return {number} 返回以秒为单位的时间
     */
    getTimeSecond(dt1, dt2) {
        return this.dateParse(dt1, dt2) / 1000
    }

    /**
     * 格式化时间字符串为yyyy-MM-dd
     * @param dt 时间字符串
     * @return {*} 返回一个yyyy-MM-dd字符串时间
     */
    formatTime(dt) {
        return dt.split("T")[0]
    }

}

export default TimeProcessor
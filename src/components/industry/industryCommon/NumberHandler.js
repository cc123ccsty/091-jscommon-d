class NumberHandler {
    /**
     *
     * @param num 数字
     * @param digits 保留几位小数
     * @return {string} 返回处理后的小数
     */
    keepDecimal = (num, digits) => {
        return parseInt(num).toFixed(digits)
    }
}

export default NumberHandler
class BuildArrayEnvironment {
    /**
     * 直接构建一个数组,数组内只会添加某一个元素属性
     * @param dataSource 数据源
     * @param attribute  需要遍历的数据属性
     * @param insiderAttr 数据属性内部的属性,作为添加进入数组中的元素
     */
    buildASimpleArrayEnvironment(dataSource, attribute, insiderAttr) {
        let arr = []
        dataSource.map(item => {
            item[attribute].map(i => {
                arr.push(i[insiderAttr])
            })
        })
        return arr
    }
}

export default BuildArrayEnvironment